#!/usr/bin/env python3
import pymongo
from bson import ObjectId
from bottle import get, post, put, run, request, debug, abort
import oauth2client
from oauth2client import client
import uuid
import OpenSSL
from datetime import datetime


# TODO: Get this from env?
CLIENT_ID         = "567760514203-i2lvj8k4bdev00dqnf10sm6s9urgk2n3.apps.googleusercontent.com"
ANDROID_CLIENT_ID = "567760514203-i2lvj8k4bdev00dqnf10sm6s9urgk2n3.apps.googleusercontent.com"


mongo    = pymongo.MongoClient("mongo", 27017)
sessions = mongo.ataxi.sessions
drivers  = mongo.ataxi.drivers
clients  = mongo.ataxi.clients

# driver: {
#   name:           <str>
#   surname:        <str>
#   phone:          <str>
#   google_id:      <str>
#   rating:         <float>
#   description:    <str>
#   status:         <str>
#   location: {
#       lng:        <float>
#       lat:        <float>
#   }
# }

# client: {
#   name:           <str>
#   surname:        <str>
#   phone:          <str>
#   google_id:      <str>
#   rating:         <float>
# }

# {
#    "_id" : ObjectId("5742c4216826b8c32930ed17"),
#    "name" : "Eustaquio",
#    "surname" : "Ramirex Perex",
#    "phone" : "555-5555-55",
#    "email" : "rape@gmail.com",
#    "status" : "working",
#    "location" : {
#        "lat" : 1.4,
#        "lng" : 2.3
#    }
# }

clients.create_index("name")
clients.create_index("google_id")
drivers.create_index("google_id")
drivers.create_index("location.lat")
drivers.create_index("location.lng")
sessions.create_index("token")
sessions.create_index("google_id")
sessions.create_index("timestamp", expireAfterSeconds=1800)


def serialize_ObjectId(item):
    item['_id'] = str(item['_id'])
    return item


def update_mongo_doc(collection, query, update_query):
    try:
        return serialize_ObjectId(
            collection.find_one_and_update(
                query,
                update_query,
                return_document=pymongo.ReturnDocument.AFTER
            )
        )
    except:
        return None


def id_query(ID):
    return {"_id": ObjectId(ID)}


def validate_google_token(token):
    idinfo = client.verify_id_token(token, CLIENT_ID)
    if idinfo['aud'] not in [ANDROID_CLIENT_ID]:
        raise oauth2client.crypt.AppIdentityError("unrecognized client")
    if idinfo['iss'] not in ['accounts.google.com', 'https://accounts.google.com']:
        raise oauth2client.crypt.AppIdentityError("wrong issuer")

    return idinfo


def validate_session(headers):
    tok = headers.get("X-Auth-Token")
    uid = headers.get("X-Auth-UserId")

    try:
        session = sessions.find_one({
            "google_id": uid,
            "token": uuid.UUID(tok)
        })
    except:
        abort(403, "Invalid session")

    if session:
        # Update session to prevent premature expiration
        sessions.find_one_and_update({"_id": session}, {
            "$set": {
                "timestamp": datetime.now()
            }
        })
        return True
    else:
        abort(403, "Invalid session")


def find_user(uid):
    driver = drivers.find_one({
        "google_id": uid
    })

    client = clients.find_one({
        "google_id": uid
    })

    return driver or client


def create_session(uid):
    session = sessions.find_one({"google_id": uid})
    if session:
        return {"token": str(session["token"])}
    else:
        token = uuid.UUID(bytes=OpenSSL.rand.bytes(16))
        sessions.insert({"google_id": uid, "token": token, "timestamp": datetime.now()})
        return {"token": str(token)}


@post('/api/v1/auth')
def auth():
    token = request.json.get("token")
    try:
        idinfo = validate_google_token(token)
        return create_session(idinfo["sub"])

    except oauth2client.crypt.AppIdentityError:
        abort(401, "Invalid token.")


def _get_driver(driver_id):
    return drivers.find_one(id_query(driver_id))


@get('/api/v1/drivers/')
def get_all_drivers_():
    return get_all_drivers()


@get('/api/v1/drivers/<page:int>')
def get_all_drivers(page=1):
    """
    Returns the list of all drivers.
    """
    validate_session(request.headers)

    drivers_list = drivers.find().skip((page - 1) * 50).limit(50)

    return {
        "total_items": drivers_list.count(),
        "items": [serialize_ObjectId(driver) for driver in drivers_list]
    }


@get('/api/v1/drivers/<driver_id>')
def get_driver(driver_id):
    validate_session(request.headers)

    return serialize_ObjectId(_get_driver(driver_id))


@post("/api/v1/drivers/new")
def register_driver():
    validate_session(request.headers)

    try:
        uid         = request.headers.get("X-Auth-UserId")  # We can trust the user because it's been validated
        name        = request.json.get("name")
        surname     = request.json.get("surname")
        phone       = request.json.get("phone")
        description = request.json.get("description")
    except:
        abort(400, "Missing required JSON parameter.")

    if drivers.find_one({"google_id": uid}):
        abort(400, "User already exists.")

    # TODO: Validate user input
    driver_id = drivers.insert({
        "google_id":    uid,
        "name":         name,
        "surname":      surname,
        "phone":        phone,
        "rating":       -1.0,
        "description":  description,
        "status": "",
        "location": {
            "lat": 0.0,
            "lng": 0.0,
        }
    })

    return serialize_ObjectId(_get_driver(driver_id))


@get('/api/v1/drivers/nearby/<lat:float>,<lng:float>')
def get_drivers_nearby(lat, lng):
    """
    Returns a list of drivers that are within 0.01º of lat and lng.
    """
    validate_session(request.headers)

    query = {
        "location.lat": {"$gte": lat - 0.01, "$lte": lat + 0.01},
        "location.lng": {"$gte": lng - 0.01, "$lte": lng + 0.01}
    }

    nearby_drivers = map(
        serialize_ObjectId,
        list(drivers.find(query))
    )

    return {
        "total_items": drivers.find(query).count(),
        "items": list(nearby_drivers)
    }


@put('/api/v1/drivers/<driver_id>/status')
def update_driver_status(driver_id):
    validate_session(request.headers)

    update_query = {
        "$set": {
            "status": request.json.get("status")
        }
    }

    return update_mongo_doc(
        drivers,
        id_query(driver_id),
        update_query
    )


@put('/api/v1/drivers/<driver_id>/location')
def update_driver_location(driver_id):
    validate_session(request.headers)

    lat = float(request.json.get("lat"))
    lng = float(request.json.get("lng"))
    query = {
        "_id": ObjectId(driver_id),
        "google_id": request.json.get("AUTH_UID")
    }
    update_query = {
        "$set": {
            "location.lat": lat,
            "location.lng": lng
        }
    }

    return update_mongo_doc(
        drivers,
        query,
        update_query
    )


@post('/api/v1/drivers/<driver_id>/request')
def request_driver(driver_id):
    pass


def _get_user(user_id):
    return clients.find_one(id_query(user_id))


@get('/api/v1/drivers/')
def get_all_users_():
    return get_all_users()


@get('/api/v1/drivers/<page:int>')
def get_all_users(page=1):
    """
    Returns the list of all drivers.
    """
    validate_session(request.headers)

    users_list = clients.find().skip((page - 1) * 50).limit(50)

    return {
        "total_items": users_list.count(),
        "items": [serialize_ObjectId(user) for user in users_list]
    }


@post("/api/v1/users/new")
def register_user():
    validate_session(request.headers)

    try:
        uid         = request.headers.get("X-Auth-UserId")
        name        = request.json.get("name")
        surname     = request.json.get("surname")
        phone       = request.json.get("phone")
    except:
        abort(400, "Missing required JSON parameter.")

    if clients.find_one({"google_id": uid}):
        abort(400, "User already exists.")

    # TODO: Validate user input
    client_id = clients.insert({
        "google_id":    uid,
        "name":         name,
        "surname":      surname,
        "phone":        phone,
        "rating":       -1.0,
    })

    return serialize_ObjectId(_get_user(client_id))


debug(True)
run(host='0.0.0.0', port=5000)
